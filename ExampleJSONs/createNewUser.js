{
    // Reference to an entry of the contact-detail endpoint of the API 
    // You can also use just the primary key (integer)
    "contactDetails": "https://easyverein.com/api/stable/contact-details/64213/",
    "email": "example@software-design.de",
    "_profilePicture": null,
    "joinDate": "2020-09-07T00:00:00.470891+02:00",
    "resignationDate": null,
    "_isChairman": false,
    "_chairmanPermissionGroup": null,
    "declarationOfApplication": null,
    "declarationOfResignation": null,
    "declarationOfConsent": null,
    "membershipNumber": "21",
    "_paymentStartDate": "2020-09-07T00:00:00.470530+02:00",
    "paymentAmount": "13.37",
    "paymentIntervallMonths": 1,
    "useBalanceForMembershipFee": false,
    "bulletinBoardNewPostNotification": false,
    "integrationDosbSport": [],
    "integrationDosbGender": null,
    "_isApplication": false,
    "_relatedMember": null,
    "sepaMandateFile": null
}
