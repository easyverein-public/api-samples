Der hier zur Verfügung gestellte Code dient lediglich der Einarbeitung in die REST-API.

Alle Codebeispiele können ggf. später für die Doku überarbeitet werden.

Ab v2.0 der API werden neue API Schlüssel verwendet, welche nach Ablauf einer bestimmten Zeit erneuert werden müssen.
Hierzu gibt es ein spezielles Beispiel [hier](https://gitlab.com/easyverein-public/api-samples/-/blob/master/Python3/refreshToken.py?ref_type=heads)

---

The code provided here is only for familiarization with the REST API.

All code examples can be revised later for the documentation.

From v2.0 of the API, new API tokens are used, which must be renewed after a certain period of time.
There is a special example of this [here](https://gitlab.com/easyverein-public/api-samples/-/blob/master/Python3/refreshToken.py?ref_type=heads)
